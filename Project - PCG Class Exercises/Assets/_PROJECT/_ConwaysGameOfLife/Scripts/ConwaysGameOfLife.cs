using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ConwaysGameOfLife : MonoBehaviour {

    public static ConwaysGameOfLife Instance;

    [SerializeField] InputActionReference selectAction;
    [SerializeField] InputActionReference mousePositionAction;
    Vector2 mousePosition;
    bool selectingCells = false;

    new Camera camera;
    bool simulating;
    float waitTime;

    void Awake () {
        Instance = this;
    }

    void Start () {
        camera = Camera.main;

        selectAction.action.Enable ();
        selectAction.action.started += x => {
            StartCoroutine (SelectingCells ());
        };
        selectAction.action.canceled += x => {
            selectingCells = false;
        };

        mousePositionAction.action.Enable ();
        mousePositionAction.action.performed += x => {
            mousePosition = x.ReadValue<Vector2> ();
        };
    }

    IEnumerator SelectingCells () {
        selectingCells = true;
        bool firstSelect = true;
        bool targetState = false;
        while (selectingCells) {
            Vector3 worldPoint = camera.ScreenToWorldPoint (mousePosition);
            worldPoint = GridGenerator.Instance.transform.InverseTransformPoint (worldPoint); //World > Local Space 
            worldPoint = worldPoint.SnapPosition ();

            if (worldPoint.x > GridGenerator.Instance.gridSize.x - 1 || worldPoint.x < 0) goto skip;
            if (worldPoint.y > GridGenerator.Instance.gridSize.y - 1 || worldPoint.y < 0) goto skip;

            int childIndex = (int) worldPoint.x + ((int) worldPoint.y * GridGenerator.Instance.gridSize.y);

            if (firstSelect) {
                targetState = !GridGenerator.Instance.cells[childIndex].alive;
                firstSelect = false;
            }

            GridGenerator.Instance.cells[childIndex].SetState (targetState);

            skip:
                yield return null;
        }
    }

    /* 
        UI
    */

    public void StartSimulation () {
        StartCoroutine (GameOfLife ());
    }

    public void StopSimulation () {
        simulating = false;
    }

    public void ResetSimulation () {
        for (var i = 0; i < GridGenerator.Instance.cells.Count; i++) {
            GridGenerator.Instance.cells[i].SetState (false);
        }
    }

    public void SetWaitTime (float time) {
        waitTime = time;
    }

    /* 
        SIMULATION
    */

    IEnumerator GameOfLife () {
        simulating = true;
        while (simulating) {
            for (var i = 0; i < GridGenerator.Instance.cells.Count; i++) {

                Cell cell = GridGenerator.Instance.cells[i];

                int liveNeighbours = 0;
                for (var k = 0; k < cell.neighbourhood.Count; k++) {
                    if (cell.neighbourhood[k].alive) liveNeighbours++;
                }

                // Rule 1 : Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                if (cell.alive && liveNeighbours < 2) {
                    cell.SetNextGeneration (false);
                }

                // Rule 2 : Any live cell with two or three live neighbours lives on to the next generation.
                if (cell.alive && (liveNeighbours == 2 || liveNeighbours == 3)) {
                    cell.SetNextGeneration (true);
                }

                // Rule 3 : Any live cell with more than three live neighbours dies, as if by overpopulation.
                if (cell.alive && liveNeighbours > 3) {
                    cell.SetNextGeneration (false);
                }

                // Rule 4 : Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                if (!cell.alive && liveNeighbours == 3) {
                    cell.SetNextGeneration (true);
                }

            }

            yield return new WaitForSeconds (waitTime);
        }
    }

}