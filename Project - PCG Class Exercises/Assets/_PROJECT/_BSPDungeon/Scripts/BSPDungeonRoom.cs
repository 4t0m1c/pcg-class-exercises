using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSPDungeonRoom : MonoBehaviour {

    [SerializeField] GameObject finalRoom;
    [SerializeField] MeshRenderer outlineMeshRenderer;
    Vector3 originalPosition = new Vector3 (0.25f, 0, 0.25f);

    internal void DisableOutline () {
        outlineMeshRenderer.enabled = false;
    }

    internal void GenerateFinalRoom () {
        finalRoom.SetActive (true);
        finalRoom.transform.localPosition += new Vector3 () {
            x = Random.Range (-0.25f, 0.25f),
            y = 0f,
            z = Random.Range (-0.25f, 0.25f),
        };
    }

    void OnDisable () {
        finalRoom.SetActive (false);
        finalRoom.transform.localPosition = originalPosition;
        outlineMeshRenderer.enabled = true;
    }
}