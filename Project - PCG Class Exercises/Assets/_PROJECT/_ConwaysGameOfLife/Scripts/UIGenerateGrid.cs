using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGenerateGrid : MonoBehaviour {

    [SerializeField] InputField inputField;

    public void GenerateGrid () {
        GridGenerator.Instance.GenerateGrid (int.Parse (inputField.text));
    }

}