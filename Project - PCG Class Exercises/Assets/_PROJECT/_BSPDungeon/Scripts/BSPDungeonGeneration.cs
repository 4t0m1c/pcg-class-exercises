using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class BSPDungeonGeneration : MonoBehaviour {

    [Header ("Parameters")]
    [SerializeField] Vector2Int mapSize;
    [SerializeField] Vector2 minRoomSize;
    [SerializeField] int generationDepth = 3;
    [SerializeField] int seed;
    [SerializeField] bool randomSeed;
    [SerializeField] bool hideOutlines;

    [Header ("References")]
    [SerializeField] PoolBSPDungeonRoom poolBSPDungeonRooms;

    [Header ("Diagnostics")]
    [SerializeField] Partition rootPartition;
    [SerializeField] List<BSPDungeonRoom> rooms = new List<BSPDungeonRoom> ();
    [SerializeField] List<BSPDungeonRoom> finalRooms = new List<BSPDungeonRoom> ();

    [Button]
    void Generate () {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            UnityEditor.EditorApplication.EnterPlaymode ();
            return;
        }
#endif

        ClearRooms ();

        rootPartition = new Partition ();
        rootPartition.partitionSize = new Rect (0, 0, mapSize.x, mapSize.y);

        Partition currentPartition = rootPartition;
        currentPartition.room = SpawnRoom (rootPartition.partitionSize, transform);

        List<Partition> childPartitions = new List<Partition> ();
        List<Partition> nextPartitions = new List<Partition> ();

        childPartitions.Add (currentPartition);

        if (!randomSeed) Random.InitState (seed);

        bool splitHorizontally = Random.Range (0, 2) == 0 ? true : false;

        for (var i = 0; i < generationDepth; i++) {
            for (var j = 0; j < childPartitions.Count; j++) {
                currentPartition = childPartitions[j];
                // splitHorizontally = Random.Range (0, 2) == 0 ? true : false;
                splitHorizontally = !splitHorizontally;

                Rect a_ = new Rect ();
                Rect b_ = new Rect ();

                if (splitHorizontally) {
                    float splitPosition = (currentPartition.partitionSize.height / 2) +
                        Random.Range (-currentPartition.partitionSize.height / 4, currentPartition.partitionSize.height / 4);

                    SplitHorizontally (currentPartition, splitPosition, out a_, out b_);
                } else {
                    float splitPosition = (currentPartition.partitionSize.width / 2) +
                        Random.Range (-currentPartition.partitionSize.width / 4, currentPartition.partitionSize.width / 4);

                    SplitVertically (currentPartition, splitPosition, out a_, out b_);
                }

                if (CheckRoomSize (a_) && CheckRoomSize (b_) && i < generationDepth - 1) {
                    //More partitions to come
                    CreatePartition (ref currentPartition, ref nextPartitions, ref a_);
                    CreatePartition (ref currentPartition, ref nextPartitions, ref b_);
                } else {
                    //Final room
                    finalRooms.Add (currentPartition.room);
                    currentPartition.room.GenerateFinalRoom ();
                }
            }

            childPartitions = nextPartitions;
            nextPartitions = new List<Partition> ();
        }

    }

    void CreatePartition (ref Partition currentPartition, ref List<Partition> nextPartitions, ref Rect rect) {
        Partition partition = new Partition ();
        partition.partitionSize = rect;
        partition.room = SpawnRoom (partition.partitionSize, currentPartition.room.transform);
        nextPartitions.Add (partition);
        currentPartition.childPartitions.Add (partition);
    }

    bool CheckRoomSize (Rect rect) {
        return rect.width >= minRoomSize.x && rect.height >= minRoomSize.y;
    }

    void SplitVertically (Partition currentPartition, float splitPosition, out Rect left, out Rect right) {
        left = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y,
            width = splitPosition,
            height = currentPartition.partitionSize.height
        };

        right = new Rect () {
            x = currentPartition.partitionSize.x + splitPosition,
            y = currentPartition.partitionSize.y,
            width = currentPartition.partitionSize.width - splitPosition,
            height = currentPartition.partitionSize.height
        };
    }

    void SplitHorizontally (Partition currentPartition, float splitPosition, out Rect bottom, out Rect top) {
        bottom = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y,
            width = currentPartition.partitionSize.width,
            height = splitPosition
        };

        top = new Rect () {
            x = currentPartition.partitionSize.x,
            y = currentPartition.partitionSize.y + splitPosition,
            width = currentPartition.partitionSize.width,
            height = currentPartition.partitionSize.height - splitPosition
        };
    }

    void ClearRooms () {
        finalRooms.Clear ();
        for (var i = 0; i < rooms.Count; i++) {
            poolBSPDungeonRooms.PutIntoObjectPool (rooms[i]);
        }
        rooms.Clear ();
    }

    BSPDungeonRoom SpawnRoom (Rect partitionSize, Transform roomParent) {
        BSPDungeonRoom newRoom = poolBSPDungeonRooms.GetFromObjectPool ();
        newRoom.transform.localScale = new Vector3 (partitionSize.width, 1, partitionSize.height);
        newRoom.transform.localPosition = new Vector3 (partitionSize.x, 0, partitionSize.y);
        newRoom.transform.SetParent (roomParent, true);
        rooms.Add (newRoom);

        if (hideOutlines) newRoom.DisableOutline ();

        return newRoom;
    }
}

[System.Serializable]
public class Partition {
    public Rect partitionSize;
    public List<Partition> childPartitions = new List<Partition> ();
    public BSPDungeonRoom room;
}