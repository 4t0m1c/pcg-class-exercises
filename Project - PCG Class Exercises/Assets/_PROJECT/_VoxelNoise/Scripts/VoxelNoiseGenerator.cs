using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VoxelNoiseGenerator : MonoBehaviour {

    [SerializeField] RawImage noiseOutput;
    [SerializeField] int textureSize;
    [SerializeField] int terrainSize;
    [SerializeField] int maxHeight;
    [SerializeField] float noiseScale;
    [SerializeField] int seed;
    [SerializeField] bool randomiseSeed;
    Texture2D noiseTexture;

    [SerializeField] PoolVoxelBlock poolGrassBlock;
    [SerializeField] PoolVoxelBlock poolDirtBlock;

    List<VoxelBlock> blocksGrass = new List<VoxelBlock> ();
    List<VoxelBlock> blocksDirt = new List<VoxelBlock> ();

    public void GenerateNoise () {
        noiseTexture = new Texture2D (textureSize, textureSize);
        float pixelValue = 0;

        if (randomiseSeed) seed = Random.Range (0, int.MaxValue);
        System.Random rng = new System.Random (seed);
        float offset = (float) rng.NextDouble () * rng.Next (100);

        for (int height = 0; height < textureSize; height++) {
            for (int width = 0; width < textureSize; width++) {

                float xCoord = offset + ((float) width / (float) textureSize) * noiseScale;
                float yCoord = offset + ((float) height / (float) textureSize) * noiseScale;

                pixelValue = Mathf.PerlinNoise (xCoord, yCoord);

                noiseTexture.SetPixel (width, height, new Color (pixelValue, pixelValue, pixelValue));
            }
        }

        noiseTexture.Apply ();
        noiseOutput.texture = noiseTexture;
    }

    public void GenerateVoxels () {
        ClearVoxels ();

        for (int height = 0; height < terrainSize; height++) {
            for (int width = 0; width < terrainSize; width++) {

                int xCoord = Mathf.FloorToInt (((float) width * (float) textureSize) / (float) terrainSize);
                int yCoord = Mathf.FloorToInt (((float) height * (float) textureSize) / (float) terrainSize);

                float pixelValue = noiseTexture.GetPixel (xCoord, yCoord).r; // 0f..1f
                float blockHeight = pixelValue * maxHeight;
                blockHeight = Mathf.Round (blockHeight);

                VoxelBlock newBlock = poolGrassBlock.GetFromObjectPool ();
                newBlock.transform.position = new Vector3 (width, blockHeight, height);
                blocksGrass.Add (newBlock);

                if (blockHeight > 0) {
                    for (var i = 0; i < blockHeight; i++) {
                        newBlock = poolDirtBlock.GetFromObjectPool ();
                        newBlock.transform.position = new Vector3 (width, i, height);
                        blocksDirt.Add (newBlock);
                    }
                }
            }
        }

    }

    void ClearVoxels () {
        for (var i = 0; i < blocksGrass.Count; i++) {
            poolGrassBlock.PutIntoObjectPool (blocksGrass[i]);
        }
        blocksGrass.Clear ();

        for (var i = 0; i < blocksDirt.Count; i++) {
            poolDirtBlock.PutIntoObjectPool (blocksDirt[i]);
        }
        blocksDirt.Clear ();
    }

}