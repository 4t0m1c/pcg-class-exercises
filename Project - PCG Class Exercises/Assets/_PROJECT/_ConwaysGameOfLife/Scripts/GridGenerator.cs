using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    public static GridGenerator Instance;

    [Header("References")]
    [SerializeField] PoolCell poolCell;

    [Header("Debug")]
    public Vector2Int gridSize;
    public List<Cell> cells = new List<Cell> ();

    new Camera camera;

    void Awake () {
        Instance = this;

        camera = Camera.main;
    }

    public void GenerateGrid (int _gridSize) {
        gridSize = Vector2Int.one * _gridSize;

        ClearCells ();

        for (var h = 0; h < gridSize.y; h++) {
            for (var w = 0; w < gridSize.x; w++) {

                Cell newCell = poolCell.GetFromObjectPool ();
                newCell.name = $"[{w},{h}]";
                cells.Add (newCell);

                newCell.transform.localPosition = new Vector3 (w, h, 0);

                //Set start state
                newCell.SetState (false);

                //Set coordinate
                newCell.SetCoordinate (new Vector2Int (w, h));
            }
        }

        for (var i = 0; i < cells.Count; i++) {
            List<Cell> neighbourhood = new List<Cell> ();
            int x = cells[i].coordinate.x;
            int y = cells[i].coordinate.y;

            //N
            neighbourhood.Add (cells.GetCell (new Vector2Int (x, y + 1), gridSize));
            //NE
            neighbourhood.Add (cells.GetCell (new Vector2Int (x + 1, y + 1), gridSize));
            //E
            neighbourhood.Add (cells.GetCell (new Vector2Int (x + 1, y), gridSize));
            //SE
            neighbourhood.Add (cells.GetCell (new Vector2Int (x + 1, y - 1), gridSize));
            //S
            neighbourhood.Add (cells.GetCell (new Vector2Int (x, y - 1), gridSize));
            //SW
            neighbourhood.Add (cells.GetCell (new Vector2Int (x - 1, y - 1), gridSize));
            //W
            neighbourhood.Add (cells.GetCell (new Vector2Int (x - 1, y), gridSize));
            //NW
            neighbourhood.Add (cells.GetCell (new Vector2Int (x - 1, y + 1), gridSize));

            cells[i].SetNeighbourhood (neighbourhood);
        }

        RepositionCamera ();
    }

    void ClearCells () {
        for (var i = 0; i < cells.Count; i++) {
            poolCell.PutIntoObjectPool (cells[i]);
        }
        cells.Clear ();
    }

    void RepositionCamera () {
        camera.orthographicSize = gridSize.y / 1.75f;
        transform.position = new Vector3 (-gridSize.x / 2, (-gridSize.y / 2) + 1, 0);
    }
}

public static class CellExtensions {

    public static Cell GetCell (this List<Cell> cells, Vector2Int coordinate, Vector2Int gridSize) {
        //Correct out-of-bounds
        if (coordinate.x > gridSize.x - 1) coordinate.x = 0;
        if (coordinate.x < 0) coordinate.x = gridSize.x - 1;
        if (coordinate.y > gridSize.y - 1) coordinate.y = 0;
        if (coordinate.y < 0) coordinate.y = gridSize.y - 1;

        return cells[coordinate.x + (coordinate.y * gridSize.y)];
    }

    public static Vector3 SnapPosition (this Vector3 position) {
        position = new Vector3 (
            Mathf.Round (position.x),
            Mathf.Round (position.y),
            Mathf.Round (position.z)
        );
        return position;
    }

}