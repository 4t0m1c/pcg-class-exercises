using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Color colourAlive;
    [SerializeField] Color colourDead;

    [Header ("Debug")]
    public Vector2Int coordinate;
    public List<Cell> neighbourhood = new List<Cell> ();

    public bool alive = false;

    void Flip () {
        alive = !alive;
        spriteRenderer.color = alive ? colourAlive : colourDead;
    }

    internal void SetState (bool state) {
        if (alive != state) Flip ();
    }

    internal void SetCoordinate (Vector2Int coordinate) {
        this.coordinate = coordinate;
    }

    internal void SetNeighbourhood (List<Cell> neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    internal void SetNextGeneration (bool alive) {
        StartCoroutine (ApplyNextGeneration (alive));
    }

    IEnumerator ApplyNextGeneration (bool state) {
        yield return new WaitForEndOfFrame ();
        SetState (state);
    }
}