using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenerateLSystem : MonoBehaviour {

    [Header ("References")]
    [SerializeField] Transform turtle;
    [SerializeField] LineRendererObjectPool turtlePond;
    new Camera camera;

    [Header ("L-System")]
    [SerializeField] string axiom;
    [SerializeField] List<Grammar> grammars = new List<Grammar> ();

    [Header ("Settings")]
    [SerializeField] int generations = 3;
    [SerializeField] float lineLength = 0.5f;
    [SerializeField] float rotationAngle = 5;
    [SerializeField, Range (0, 10)] float frameWait = 0;
    [SerializeField, Range (0, 100)] int stepsPerFrame = 1;

    [Header ("Output")]
    [SerializeField, TextArea (3, 10)] string sequence;
    [SerializeField] bool drawingSequence = false;

    void Start () {
        camera = Camera.main;
        camera.orthographicSize = 10;
        transform.position = -Vector3.up * (camera.orthographicSize / 2);
    }

    [EasyButtons.Button]
    public void Generate () {
        drawingSequence = true;
        StartCoroutine (GenerateSequence (
            () => StartCoroutine (DrawSequence (sequence))
        ));
    }

    [EasyButtons.Button]
    public void StopGenerate () {
        drawingSequence = false;
    }

    IEnumerator GenerateSequence (UnityAction OnComplete) {
        sequence = axiom;

        for (var i = 0; i < generations; i++) {
            sequence = ProcessSequence (sequence);
            // yield return new WaitForSeconds (1);
        }

        yield return null;
        OnComplete.Invoke ();
    }

    string ProcessSequence (string sequence) {
        string processedSequence = string.Empty;

        char[] characters = sequence.ToCharArray ();

        for (var i = 0; i < characters.Length; i++) {
            bool found = false;

            for (var g = 0; g < grammars.Count; g++) {
                if (characters[i] == grammars[g].variable) {
                    processedSequence += grammars[g].rule;

                    found = true;
                    break;
                }
            }

            if (!found) {
                processedSequence += characters[i];
            }
        }

        return processedSequence;
    }

    IEnumerator DrawSequence (string sequence) {
        int stepCount = 0;

        char[] characters = sequence.ToCharArray ();

        turtle.localPosition = Vector3.zero;
        turtle.localEulerAngles = Vector3.zero;

        List<LSystemLine> lines = new List<LSystemLine> ();
        LSystemLine line = turtlePond.GetFromObjectPool ();
        lines.Add (line);

        Stack<PosRot> posRotMemory = new Stack<PosRot> ();

        for (var i = 0; i < characters.Length; i++) {

            // LSystem_Koch (characters[i]);
            // LSystem_BinaryTree (characters[i], ref posRotMemory, ref lines);
            LSystem_FractalPlant (characters[i], ref posRotMemory, ref lines);

            line = lines[lines.Count - 1];
            line.lineRenderer.positionCount++;
            line.lineRenderer.SetPosition (line.lineRenderer.positionCount - 1, turtle.localPosition);

            //Tweak Camera
            if (turtle.localPosition.y > camera.orthographicSize * 2) {
                camera.orthographicSize = ((int) turtle.localPosition.y / 2) + 1;
                transform.position = new Vector3 (0, -camera.orthographicSize, 0);
            }

            stepCount++;
            if (stepCount >= stepsPerFrame) {
                stepCount = 0;

                yield return new WaitForSeconds (frameWait);
            }

            if (!drawingSequence) break;
        }

        while (drawingSequence) {
            yield return null;
        }

        if (!drawingSequence && lines.Count > 0) {
            for (var i = 0; i < lines.Count; i++) {
                turtlePond.PutIntoObjectPool (lines[i]);
            }

            camera.orthographicSize = 10;
            transform.position = -Vector3.up * (camera.orthographicSize / 2);
        }

        yield return null;
    }

    void LSystem_Koch (char _char) {
        switch (_char) {
            case 'F': //Forward
                // Debug.Log ($"Forward");
                turtle.Translate (Vector3.up * lineLength);
                break;
            case '+': //Turn Left
                // Debug.Log ($"Turn Left");
                turtle.Rotate (Vector3.forward * -rotationAngle);
                break;
            case '-': //Minus    //Turn Right
            case '−': //En Dash  //Turn Right
                // Debug.Log ($"Turn Right");
                turtle.Rotate (Vector3.forward * rotationAngle);
                break;
            default:
                Debug.Log ($"Character not found {_char}");
                break;
        }
    }

    void LSystem_BinaryTree (char _char, ref Stack<PosRot> posRotMemory, ref List<LSystemLine> lines) {
        switch (_char) {
            case '0': //Forward
            case '1': //Forward
                // Debug.Log ($"Forward");
                turtle.Translate (Vector3.up * lineLength);
                break;
            case '[': //Save & Turn Left
                // Debug.Log ($"Save & Turn Left");
                posRotMemory.Push (new PosRot (turtle.localPosition, turtle.rotation));
                turtle.Rotate (Vector3.forward * -rotationAngle);
                break;
            case ']': //Load & Turn Right
                // Debug.Log ($"Turn Right");
                PosRot cachedPosRot = posRotMemory.Pop ();
                turtle.localPosition = cachedPosRot.position;
                turtle.rotation = cachedPosRot.rotation;

                LSystemLine line = turtlePond.GetFromObjectPool ();
                lines.Add (line);

                turtle.Rotate (Vector3.forward * rotationAngle);
                break;
            default:
                Debug.Log ($"Character not found {_char}");
                break;
        }
    }

    void LSystem_FractalPlant (char _char, ref Stack<PosRot> posRotMemory, ref List<LSystemLine> lines) {
        /* 
            Fractal Plant
        */
        switch (_char) {
            case 'F': //Forward
                Debug.Log ($"Forward");
                turtle.Translate (Vector3.up * lineLength);
                break;
            case '+': //Turn Left
                Debug.Log ($"Turn Left");
                turtle.localEulerAngles -= new Vector3 (0, 0, rotationAngle);
                break;
            case '-': //minus   //Turn Right
            case '−': //dash    //Turn Right
                Debug.Log ($"Turn Right");
                turtle.localEulerAngles += new Vector3 (0, 0, rotationAngle);
                break;
            case '[':
                Debug.Log ($"Save");
                posRotMemory.Push (new PosRot (turtle.localPosition, turtle.rotation));
                break;
            case ']':
                Debug.Log ($"Restore");
                PosRot cachedPosRot = posRotMemory.Pop ();
                turtle.localPosition = cachedPosRot.position;
                turtle.rotation = cachedPosRot.rotation;

                LSystemLine line = turtlePond.GetFromObjectPool ();
                lines.Add (line);
                break;
            case 'X':
            default:
                Debug.Log ($"Character not found! [{_char}]");
                break;
        }
    }
}

[System.Serializable]
public class Grammar {
    public char variable;
    public string rule;
}

[System.Serializable]
public struct PosRot {
    public Vector3 position;
    public Quaternion rotation;

    public PosRot (Vector3 position, Quaternion rotation) {
        this.position = position;
        this.rotation = rotation;
    }
}