using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probability : MonoBehaviour {

    public List<EnemyProbabilityValue> enemyProbabilityValues = new List<EnemyProbabilityValue> ();

    [Header ("Debug")]
    [SerializeField] float totalProbability;

    public void RollNumber (int numberOfRolls) {
        enemyProbabilityValues.ForEach (x => x.rollCount = 0);
        enemyProbabilityValues.ForEach (x => x.probabilityLikelinessOffset = 0);

        for (var i = 0; i < numberOfRolls; i++) {
            RollNumber ();
        }

        Debug.Log ($"=======================");
        for (var i = 0; i < enemyProbabilityValues.Count; i++) {
            string colour = enemyProbabilityValues[i].enemyDifficulty == EnemyDifficulty.Easy ? "green" : enemyProbabilityValues[i].enemyDifficulty == EnemyDifficulty.Medium ? "yellow" : "red";

            Debug.Log ($"<color='{colour}'>{enemyProbabilityValues[i].enemyDifficulty}</color> rolled {((float)enemyProbabilityValues[i].rollCount)/numberOfRolls * 100f}%");
        }
        Debug.Log ($"=======================");
    }

    public void RollNumber () {
        totalProbability = 0;

        enemyProbabilityValues.ForEach (x => {
            totalProbability += x.probability + x.probabilityLikelinessOffset;
        });

        float randomNum = Random.Range (0, totalProbability);

        EnemyProbabilityValue selectedValue = null;
        float runningTotal = 0;
        bool foundCandidate = false;

        for (var i = 0; i < enemyProbabilityValues.Count; i++) {
            bool offsetValue = true;
            if (!foundCandidate) {
                runningTotal += enemyProbabilityValues[i].probability + enemyProbabilityValues[i].probabilityLikelinessOffset;

                if (randomNum <= runningTotal) {
                    selectedValue = enemyProbabilityValues[i];
                    foundCandidate = true;
                    offsetValue = false;

                    enemyProbabilityValues[i].probabilityLikelinessOffset = 0;
                    enemyProbabilityValues[i].rollCount++;

                    string colour = selectedValue.enemyDifficulty == EnemyDifficulty.Easy ? "green" : selectedValue.enemyDifficulty == EnemyDifficulty.Medium ? "yellow" : "red";

                    Debug.Log ($"Rolled {randomNum} / {runningTotal} and found <color='{colour}'>{selectedValue.enemyDifficulty}</color> enemy");
                }
            }

            if (offsetValue) {
                enemyProbabilityValues[i].probabilityLikelinessOffset += enemyProbabilityValues[i].probabilityIncrement;
            }
        }

    }

}

public enum EnemyDifficulty {
    Easy,
    Medium,
    Hard
}

[System.Serializable]
public class EnemyProbabilityValue {

    public EnemyDifficulty enemyDifficulty;
    public float probability;
    public float probabilityIncrement = 1f;

    public float probabilityLikelinessOffset;
    public int rollCount;

}