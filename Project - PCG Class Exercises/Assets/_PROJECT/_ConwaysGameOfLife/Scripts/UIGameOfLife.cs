using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameOfLife : MonoBehaviour {

    public void StartSimulation () {
        ConwaysGameOfLife.Instance.StartSimulation ();
    }

    public void StopSimulation () {
        ConwaysGameOfLife.Instance.StopSimulation ();
    }

    public void ResetSimulation () {
        ConwaysGameOfLife.Instance.ResetSimulation ();
    }

    public void SetSimulationTime (float time) {
        ConwaysGameOfLife.Instance.SetWaitTime (time);
    }

}